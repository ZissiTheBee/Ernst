package de.zissithebee.ernst.asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.database.Dish;
import de.zissithebee.ernst.database.Entry;
import de.zissithebee.ernst.database.ErnstDatabase;
import de.zissithebee.ernst.database.Medicine;
import de.zissithebee.ernst.database.MedicineTook;
import de.zissithebee.ernst.database.Note;
import de.zissithebee.ernst.database.Symptom;
import de.zissithebee.ernst.enums.EntryCategories;
import de.zissithebee.ernst.fragments.drawerFragments.ExportFragment;

public class GetEntriesForExport extends AsyncTask<ExportFragment, String, String> {

    private static final String SEPARATOR = ",";
    private static final String LINE_BREAK = "\n";

    private ExportFragment exportFragment;

    private boolean exportFood;
    private boolean exportSymptoms;
    private boolean exportMedicine;
    private boolean exportNotes;
    private Context context;

    public GetEntriesForExport(boolean exportFood, boolean exportSymptoms, boolean exportMedicine, boolean exportNotes) {
        this.exportFood = exportFood;
        this.exportSymptoms = exportSymptoms;
        this.exportMedicine = exportMedicine;
        this.exportNotes = exportNotes;
    }

    @Override
    protected String doInBackground(ExportFragment... exportFragments) {
        exportFragment = exportFragments[0];
        context = exportFragment.getContext();
        QueryHelper queryHelper = new QueryHelper(context);
        Entry[] entries = ErnstDatabase.getInstance(context)
                .entryDao()
                .getAllEntries();
        entries = queryHelper.getAllEntries(entries);
        String csv = buildStringForCSV(entries);
        return csv;
    }


    private String buildStringForCSV(Entry[] entries) {
        StringBuilder builder = new StringBuilder();
        builder.append(getStringForStartCSV());
        String currentDate = "";
        String lastDate = "";
        for (Entry entry : entries) {
            if(entry != null){
                currentDate = entry.getDate();
                if (!currentDate.equals(lastDate)) {
                    builder.append(LINE_BREAK);
                    lastDate = entry.getDate();
                }
                if (entry.getCategory().equals(EntryCategories.DISH.toString()) && exportFood) {
                    builder.append(getStringForEatenDishEntry(entry));
                } else if (entry.getCategory().equals(EntryCategories.SYMPTOM.toString()) && exportSymptoms) {
                    builder.append(getStringForSymptomEntry(entry));
                } else if (entry.getCategory().equals(EntryCategories.MEDICINE.toString()) && exportMedicine) {
                    builder.append(getStringForMedicine(entry));
                } else if (entry.getCategory().equals(EntryCategories.NOTE.toString()) && exportNotes) {
                    builder.append(getStringForNote(entry));
                }
            }
        }
        return builder.toString();
    }

    private String getStringForStartCSV() {
        StringBuilder builder = new StringBuilder();
        builder.append("sep=");
        builder.append(SEPARATOR);
        builder.append(LINE_BREAK);
        builder.append(context.getString(R.string.export_from));
        builder.append(SEPARATOR + SEPARATOR);
        builder.append(context.getString(R.string.export_to));
        builder.append(SEPARATOR + SEPARATOR);
        builder.append(context.getString(R.string.export_category));
        builder.append(LINE_BREAK);
        return builder.toString();
    }

    private String getStringForEatenDishEntry(Entry entry) {
        StringBuilder builder = new StringBuilder();
        Dish dish = entry.getDishEaten().getDish();
        builder.append(escapeCharacters(entry.getDate()));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(entry.getTime()));
        builder.append(SEPARATOR + SEPARATOR + SEPARATOR);
        builder.append(escapeCharacters(context.getString(R.string.export_category_food)));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(context.getString(dish.getDishCategoryInSelectedLanguage())));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(dish.getName()));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(dish.getIngredients()));
        builder.append(LINE_BREAK);
        return builder.toString();
    }


    private String getStringForSymptomEntry(Entry entry) {
        StringBuilder builder = new StringBuilder();
        Symptom symptom = entry.getSymptom();
        builder.append(escapeCharacters(entry.getDate()));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(entry.getTime()));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(entry.getEndDate()));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(entry.getEndTime()));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(context.getString(R.string.export_category_symptom)));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(context.getString(symptom.getSymptomCategoryInLanguage())));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(symptom.getDescription()));
        builder.append(" (");
        builder.append(symptom.getStrength());
        builder.append(")");
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(context.getString(symptom.getMedicineTookInSelectedLanguage())));
        builder.append(LINE_BREAK);
        return builder.toString();
    }

    private String getStringForMedicine(Entry entry) {
        StringBuilder builder = new StringBuilder();
        Medicine medicine = entry.getMedicineTook().getMedicine();
        MedicineTook medicineTook = entry.getMedicineTook();
        builder.append(escapeCharacters(entry.getDate()));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(entry.getTime()));
        builder.append(SEPARATOR + SEPARATOR + SEPARATOR);
        builder.append(escapeCharacters(context.getString(R.string.export_category_medicine)));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(context.getString(medicine.getMedicineCategoryInSelectedLanguage())));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(medicine.getName()));
        builder.append(" (");
        builder.append(escapeCharacters(medicineTook.getDose()));
        builder.append(")");
        builder.append(LINE_BREAK);
        return builder.toString();
    }

    private String getStringForNote(Entry entry) {
        StringBuilder builder = new StringBuilder();
        Note note = entry.getNote();
        builder.append(escapeCharacters(entry.getDate()));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(entry.getTime()));
        builder.append(SEPARATOR + SEPARATOR + SEPARATOR);
        builder.append(escapeCharacters(context.getString(R.string.export_category_note)));
        builder.append(SEPARATOR);
        builder.append(escapeCharacters(note.getNote()));
        builder.append(LINE_BREAK);
        return builder.toString();
    }

    private String escapeCharacters(String s) {
        if(s.contains(LINE_BREAK)){
            s = s.replace(LINE_BREAK, ", ");
        }
        if (s.contains(SEPARATOR)) {
            s = "\"" + s + "\"";
        }
        if (s.contains("ä") || s.contains("ö") || s.contains("ü")) {
            s = s.replace("ä", "ae");
            s = s.replace("ö", "oe");
            s = s.replace("ü", "ue");
        }
        if(s.contains("ß")){
            s = s.replace("ß", "ss");
        }
        return s;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {
            String filename = "ernst.csv";
            File exportFile = new File((exportFragment.getContext().getFilesDir()), filename);
            FileWriter fileWriter = new FileWriter(exportFile);
            fileWriter.append(s);
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception e) {
            Toast.makeText(context, context.getString(R.string.error_saving_file), Toast.LENGTH_LONG).show();
        }
        exportFragment.sendEmail();
    }
}
