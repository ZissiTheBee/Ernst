package de.zissithebee.ernst.asynctasks;

import android.content.Context;
import android.util.Log;

import de.zissithebee.ernst.database.Dish;
import de.zissithebee.ernst.database.DishEaten;
import de.zissithebee.ernst.database.Entry;
import de.zissithebee.ernst.database.ErnstDatabase;
import de.zissithebee.ernst.database.Medicine;
import de.zissithebee.ernst.database.MedicineTook;
import de.zissithebee.ernst.database.Note;
import de.zissithebee.ernst.database.Symptom;
import de.zissithebee.ernst.enums.EntryCategories;

public class QueryHelper {

    private Context context;

    public QueryHelper(Context context) {
        this.context = context;
    }

    public Entry[] getAllEntries(Entry[] entries) {
        for (int i = 0; i < entries.length; i++) {
            entries[i] = getFilledEntry(entries[i]);
        }
        return entries;
    }


    private Entry getFilledEntry(Entry entry) {
        if (entry.getCategory().equals(EntryCategories.DISH.toString())) {
            entry = setFood(entry);
        } else if (entry.getCategory().equals(EntryCategories.SYMPTOM.toString())) {
            entry = setSymptoms(entry);
        } else if (entry.getCategory().equals(EntryCategories.MEDICINE.toString())) {
            entry = setMedicine(entry);
        } else if (entry.getCategory().equals(EntryCategories.NOTE.toString())) {
            entry = setNotes(entry);
        }
        return entry;
    }

    private Entry setFood(Entry entry) {
        DishEaten dishEaten = ErnstDatabase.getInstance(context)
                .dishEatenDao()
                .getDishEatenById(entry.getId());
        if (dishEaten != null && getDish(dishEaten) != null) {
            Dish dish = getDish(dishEaten);
            dishEaten.setDish(dish);
            entry.setDishEaten(dishEaten);
            return entry;
        } else {
            return null;
        }
    }


    private Dish getDish(DishEaten dishEaten) {
        return ErnstDatabase.getInstance(context)
                .dishDao()
                .getDishById(dishEaten.getDishId());
    }

    private Entry setSymptoms(Entry entry) {
        Symptom symptom = ErnstDatabase.getInstance(context)
                .symptomDao()
                .getSymptomById(entry.getId());
        entry.setSymptom(symptom);
        return entry;
    }

    private Entry setMedicine(Entry entry) {
        MedicineTook medicineTook = ErnstDatabase.getInstance(context)
                .medicineTookDao()
                .getMedicineTookById(entry.getId());
        if (medicineTook != null && getMedicine(medicineTook) != null) {
            Medicine medicine = getMedicine(medicineTook);
            medicineTook.setMedicine(medicine);
            entry.setMedicineTook(medicineTook);
            return entry;
        } else {
            return null;
        }
    }

    private Medicine getMedicine(MedicineTook medicineTook) {
        return ErnstDatabase.getInstance(context)
                .medicineDao()
                .getMedicineById(medicineTook.getMedicineId());
    }

    private Entry setNotes(Entry entry) {
        Note note = ErnstDatabase.getInstance(context)
                .noteDao()
                .getNoteById(entry.getId());
        entry.setNote(note);
        return entry;
    }

}
