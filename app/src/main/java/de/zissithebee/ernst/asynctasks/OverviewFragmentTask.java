package de.zissithebee.ernst.asynctasks;

import android.os.AsyncTask;

import java.util.HashSet;

import de.zissithebee.ernst.database.Note;
import de.zissithebee.ernst.enums.EntryCategories;
import de.zissithebee.ernst.database.ErnstDatabase;
import de.zissithebee.ernst.fragments.drawerFragments.OverviewFragment;
import de.zissithebee.ernst.helper.ViewHandler;

public class OverviewFragmentTask extends AsyncTask<OverviewFragment, String, String> {

    private OverviewFragment overviewFragment;
    private HashSet<String> foodCategories;
    private HashSet<String> symptomCategories;
    private HashSet<String> medicineCategories;

    @Override
    protected void onProgressUpdate(String[] values) {
        super.onProgressUpdate(values);
        if (values[0].equals(EntryCategories.DISH.toString())) {
            overviewFragment.updateFoodView(foodCategories);
        } else if (values[0].equals(EntryCategories.SYMPTOM.toString())) {
            overviewFragment.updateSymptomView(symptomCategories);
        } else if (values[0].equals(EntryCategories.MEDICINE.toString())) {
            overviewFragment.updateMedicineView(medicineCategories);
        }
    }

    @Override
    protected String doInBackground(OverviewFragment... overviewFragments) {
        overviewFragment = overviewFragments[0];
        ViewHandler viewHandler = new ViewHandler(overviewFragment.getActivity());
        String date = viewHandler.getCurrentDate();
        getDishCategories(date);
        getSymptomCategories(date);
        getMedicineCategories(date);
        String lastNote = getLastNote(date);
        return lastNote;
    }

    private void getDishCategories(String date) {
        foodCategories = new HashSet<>();
        String entryCategory = EntryCategories.DISH.toString();
        float[] todaysEatenDishesIds = ErnstDatabase.getInstance(overviewFragment.getActivity())
                .entryDao()
                .getTodaysEntryByCategory(date, EntryCategories.DISH.toString());
        for (float id : todaysEatenDishesIds) {
            float[] todaysDishesIds = ErnstDatabase.getInstance(overviewFragment.getActivity())
                    .dishEatenDao()
                    .getDishIds((int) id);
            for (float id2 : todaysDishesIds) {
                String category = ErnstDatabase.getInstance(overviewFragment.getActivity())
                        .dishDao()
                        .getCategroyById((int) id2);
                foodCategories.add(category);
            }
        }
        publishProgress(entryCategory);
    }

    private void getSymptomCategories(String date) {
        symptomCategories = new HashSet<>();
        String entryCategory = EntryCategories.SYMPTOM.toString();
        float[] todaysSymptomIds = ErnstDatabase.getInstance(overviewFragment.getActivity())
                .entryDao()
                .getTodaysEntryByCategory(date, EntryCategories.SYMPTOM.toString());
        for (float id : todaysSymptomIds) {
            String category = ErnstDatabase.getInstance(overviewFragment.getActivity())
                    .symptomDao()
                    .getCategoriesById((int) id);
            symptomCategories.add(category);
        }
        publishProgress(entryCategory);
    }

    private void getMedicineCategories(String date) {
        medicineCategories = new HashSet<>();
        String entryCategory = EntryCategories.MEDICINE.toString();
        float[] todaysMedicineTookIds = ErnstDatabase.getInstance(overviewFragment.getActivity())
                .entryDao()
                .getTodaysEntryByCategory(date, EntryCategories.MEDICINE.toString());
        for (float id : todaysMedicineTookIds) {
            float[] todaysMedicineIds = ErnstDatabase.getInstance(overviewFragment.getActivity())
                    .medicineTookDao()
                    .getMedicineTookIds((int) id);
            for (float id2 : todaysMedicineIds) {
                String category = ErnstDatabase.getInstance(overviewFragment.getActivity())
                        .medicineDao()
                        .getCategroyById((int) id2);
                medicineCategories.add(category);
            }
        }
        publishProgress(entryCategory);
    }

    private String getLastNote(String date) {
        float[] todaysNoteIds = ErnstDatabase.getInstance(overviewFragment.getActivity())
                .entryDao()
                .getTodaysEntryByCategory(date, EntryCategories.NOTE.toString());
        if (todaysNoteIds.length > 0) {
            Note note = ErnstDatabase.getInstance(overviewFragment.getActivity())
                    .noteDao()
                    .getNoteById((int) todaysNoteIds[0]);
            return note.getNote();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String note) {
        overviewFragment.updateNoteView(note);
        super.onPostExecute(note);
    }
}
