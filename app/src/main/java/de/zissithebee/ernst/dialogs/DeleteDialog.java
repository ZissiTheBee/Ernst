package de.zissithebee.ernst.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import java.util.List;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.database.Dish;
import de.zissithebee.ernst.database.DishEaten;
import de.zissithebee.ernst.database.Entry;
import de.zissithebee.ernst.database.ErnstDatabase;
import de.zissithebee.ernst.database.Medicine;
import de.zissithebee.ernst.database.MedicineTook;
import de.zissithebee.ernst.database.Note;
import de.zissithebee.ernst.database.Symptom;
import de.zissithebee.ernst.enums.EntryCategories;

import static de.zissithebee.ernst.enums.Keeper.ID;
import static de.zissithebee.ernst.enums.Keeper.TIME;
import static de.zissithebee.ernst.enums.Keeper.TITLE;


public class DeleteDialog extends DialogFragment {

    private List<Fragment> fragments;
    private ErnstDatabase database;

    public DeleteDialog(List<Fragment> fragments) {
        this.fragments = fragments;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int id = 0;
        String title = "";
        String date = "";
        if (getArguments() != null) {
            id = getArguments().getInt(ID.toString());
            title = getArguments().getString(TITLE.toString());
            date = getArguments().getString(TIME.toString());
        }
        final int idToDelete = id;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.dialog_delete_title))
                .setMessage(getString(R.string.dialog_delete, title, date))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteEntry(idToDelete);
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
        return builder.create();
    }

    private void deleteEntry(final int idToDelete) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    database = ErnstDatabase.getInstance(getActivity());
                    Entry entry = database
                            .entryDao()
                            .getEntryById(idToDelete);
                    //deleteEntriesConcernedByThisEntry(entry);
                    database.entryDao()
                            .delete(entry);

                } catch (Exception e) {
                    //
                }
            }
        }).start();
        for (Fragment fragment : fragments) {
            fragment.onResume();
        }
    }
}