package de.zissithebee.ernst;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import de.zissithebee.ernst.fragments.MainFragment;
import de.zissithebee.ernst.fragments.drawerFragments.AboutFragment;
import de.zissithebee.ernst.fragments.drawerFragments.DetailsFragment;
import de.zissithebee.ernst.fragments.drawerFragments.ExportFragment;
import de.zissithebee.ernst.fragments.drawerFragments.FeedbackFragment;
import de.zissithebee.ernst.fragments.drawerFragments.SettingsFragment;
import de.zissithebee.ernst.helper.SharedPreferencesHelper;
import de.zissithebee.ernst.helper.ViewHandler;

import static de.zissithebee.ernst.enums.Keeper.SHOW_CALENDAR;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Fragment fragment;
    private ViewHandler viewHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferencesHelper sharedPreferencesHelper = new SharedPreferencesHelper(this);

        decideToShowTourActivity(sharedPreferencesHelper);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        viewHandler = new ViewHandler(this);
        viewHandler.setTextInToolbar(getString(R.string.my_day), getSupportActionBar(), false);

        View headerView = navigationView.getHeaderView(0);
        TextView tvHello = headerView.findViewById(R.id.nav_hello);
        tvHello.setText(getString(R.string.hello, sharedPreferencesHelper.getName()));

        showFirstFragment();
    }

    private void decideToShowTourActivity(SharedPreferencesHelper sharedPreferencesHelper) {
        if (sharedPreferencesHelper.getIsFirstStart()) {
            Intent intent = new Intent(this, TourActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            this.finish();
        }
    }

    private void showFirstFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragment = new MainFragment();
        fragmentTransaction.replace(R.id.frame_layout, fragment);
        fragmentTransaction.commit();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        String headline = "";
        fragment = null;
        int id = item.getItemId();

        if (id == R.id.nav_today) {
            fragment = new MainFragment();
            headline = getString(R.string.my_day);
        } else if (id == R.id.nav_calendar) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(SHOW_CALENDAR.toString(), true);
            fragment = new DetailsFragment();
            fragment.setArguments(bundle);
            headline = getString(R.string.nav_calendar);
        } else if (id == R.id.nav_tour) {
            Intent intent = new Intent(this, TourActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        } else if (id == R.id.nav_export) {
            fragment = new ExportFragment();
            headline = getString(R.string.nav_export);
        } else if (id == R.id.nav_settings) {
            fragment = new SettingsFragment();
            headline = getString(R.string.nav_settings);
        } else if (id == R.id.nav_help) {
            fragment = new FeedbackFragment();
            headline = getString(R.string.nav_help);
        } else if (id == R.id.nav_about) {
            fragment = new AboutFragment();
            headline = getString(R.string.nav_about);
        }
        if (fragment != null) {
            fragmentTransaction.replace(R.id.frame_layout, fragment);
            fragmentTransaction.commit();
            viewHandler.setTextInToolbar(headline, getSupportActionBar(), false);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
