package de.zissithebee.ernst.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


@Dao
public interface EntryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertEntry(Entry entry);

    @Query("SELECT * FROM entries WHERE id = :id")
    Entry getEntryById(int id);

    @Query("SELECT * FROM entries WHERE date = :date OR end_date = :date ORDER BY time DESC")
    Entry[] getEntriesByDate(String date);

    @Query("SELECT id FROM entries WHERE (date = :date OR end_date = :date) AND category = :categoryOfEntry ORDER BY time DESC")
    float[] getTodaysEntryByCategory(String date, String categoryOfEntry);

    @Query("SELECT * FROM entries ORDER BY date, time")
    Entry[] getAllEntries();

    @Delete
    void delete(Entry entry);

    @Update
    void updateEntry(Entry entry);

}
