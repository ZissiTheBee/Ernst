package de.zissithebee.ernst.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.enums.MedicineCategories;

@Entity(tableName = "medicines")
public class Medicine {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "name")
    @NonNull
    private String name;

    @ColumnInfo(name = "category")
    @Nullable
    private String category;

    public Medicine(String name, String category) {
        this.name = name;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getMedicineCategoryInSelectedLanguage() {
        if (category.equals(MedicineCategories.ANTIHISTAMINE.toString())) {
            return R.string.medicine_antihistamine;
        } else if (category.equals(MedicineCategories.CORTISONE.toString())) {
            return R.string.medicine_cortisone;
        } else if (category.equals(MedicineCategories.PAINKILLER.toString())) {
            return R.string.medicine_painkiller;
        } else if (category.equals(MedicineCategories.SKIN_CREAM.toString())) {
            return R.string.medicine_skin_creame;
        } else if (category.equals(MedicineCategories.GASTROINTESTINAL.toString())) {
            return R.string.medicine_gastrointestinal_medecine;
        } else if (category.equals(MedicineCategories.ASTHMA.toString())) {
            return R.string.medicine_asthma_medicine;
        } else if (category.equals(MedicineCategories.ANTIBIOTIC.toString())) {
            return R.string.medicine_antibiotic;
        } else if (category.equals(MedicineCategories.COLD_MED.toString())) {
            return R.string.medicine_cold_medicine;
        } else {
            return R.string.medicine_other;
        }
    }

    public int getMedicineCategoryImage() {
        if (category.equals(MedicineCategories.ANTIHISTAMINE.toString())) {
            return R.drawable.ic_medicine_antihistamine;
        } else if (category.equals(MedicineCategories.CORTISONE.toString())) {
            return R.drawable.ic_medicine_cortisone;
        } else if (category.equals(MedicineCategories.PAINKILLER.toString())) {
            return R.drawable.ic_medicine_painkiller;
        } else if (category.equals(MedicineCategories.SKIN_CREAM.toString())) {
            return R.drawable.ic_medicine_skin_cream;
        } else if (category.equals(MedicineCategories.GASTROINTESTINAL.toString())) {
            return R.drawable.ic_medicine_gastrointestinal_medicine;
        } else if (category.equals(MedicineCategories.ASTHMA.toString())) {
            return R.drawable.ic_medicine_asthma_medicine;
        } else if (category.equals(MedicineCategories.ANTIBIOTIC.toString())) {
            return R.drawable.ic_medicine_antibiotic;
        } else if (category.equals(MedicineCategories.COLD_MED.toString())) {
            return R.drawable.ic_medicine_cold;
        } else {
            return R.drawable.ic_medicine_other;
        }
    }

    public int getFrameForImageInAddingActivity() {
        if (category.equals(MedicineCategories.ANTIHISTAMINE.toString())) {
            return R.id.med_antihistamine;
        } else if (category.equals(MedicineCategories.CORTISONE.toString())) {
            return R.id.med_cortisone;
        } else if (category.equals(MedicineCategories.PAINKILLER.toString())) {
            return R.id.med_painkiller;
        } else if (category.equals(MedicineCategories.SKIN_CREAM.toString())) {
            return R.id.med_skin_cream;
        } else if (category.equals(MedicineCategories.GASTROINTESTINAL.toString())) {
            return R.id.med_gastrointestinal_medicine;
        } else if (category.equals(MedicineCategories.ASTHMA.toString())) {
            return R.id.med_asthma_medicine;
        } else if (category.equals(MedicineCategories.ANTIBIOTIC.toString())) {
            return R.id.med_antibiotic;
        } else if (category.equals(MedicineCategories.COLD_MED.toString())) {
            return R.id.med_cold_medicine;
        } else {
            return R.id.med_other;
        }
    }
}
