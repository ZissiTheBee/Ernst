package de.zissithebee.ernst.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;


@Database(entities = {Dish.class, DishEaten.class, Medicine.class, Intolerance.class,
        MedicineTook.class, Note.class, Symptom.class, Entry.class},
        version = 1)
public abstract class ErnstDatabase extends RoomDatabase {

    public abstract DishDao dishDao();

    public abstract DishEatenDao dishEatenDao();

    public abstract IntoleranceDao intoleranceDao();

    public abstract MedicineDao medicineDao();

    public abstract MedicineTookDao medicineTookDao();

    public abstract NoteDao noteDao();

    public abstract SymptomDao symptomDao();

    public abstract EntryDao entryDao();

    private static final String DB_NAME = "ernstDatabase.db";

    private static volatile ErnstDatabase instance;

    public static synchronized ErnstDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static ErnstDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                ErnstDatabase.class,
                DB_NAME
        ).build();
    }

}
