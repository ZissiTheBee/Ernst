package de.zissithebee.ernst.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "dishesEaten",
        foreignKeys = {@ForeignKey(entity = Dish.class, parentColumns = "id", childColumns = "dish_id", onDelete = CASCADE),
                @ForeignKey(entity = Entry.class, parentColumns = "id", childColumns = "entry_id", onDelete = CASCADE)})
public class DishEaten {

    @PrimaryKey
    @ColumnInfo(name = "entry_id")
    private int entryId;

    @ColumnInfo(name = "dish_id")
    private int dishId;

    @Ignore
    private Dish dish;

    @Ignore
    private Entry entry;

    public DishEaten(int entryId, int dishId) {
        this.entryId = entryId;
        this.dishId = dishId;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

}
