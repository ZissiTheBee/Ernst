package de.zissithebee.ernst.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.enums.SymptomCategories;

import static android.arch.persistence.room.ForeignKey.CASCADE;


@Entity(tableName = "symptoms",
        foreignKeys = @ForeignKey(entity = Entry.class, parentColumns = "id", childColumns = "entry_id", onDelete = CASCADE))
public class Symptom {

    @PrimaryKey
    @ColumnInfo(name = "entry_id")
    private int entryId;

    @ColumnInfo(name = "category")
    private String category;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "strength")
    private int strength;

    @Ignore
    private Entry entry;

    @ColumnInfo(name = "took_medicine")
    private int tookMedicine;

    public Symptom(int entryId, String category, String description, int strength, int tookMedicine) {
        this.entryId = entryId;
        this.category = category;
        this.description = description;
        this.strength = strength;
        this.tookMedicine = tookMedicine;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    @NonNull
    public String getCategory() {
        return category;
    }

    public void setCategory(@NonNull String category) {
        this.category = category;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getTookMedicine() {
        return tookMedicine;
    }

    public void setTookMedicine(int tookMedicine) {
        this.tookMedicine = tookMedicine;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public int getSymptomCategoryInLanguage() {
        if (category.equals(SymptomCategories.SKIN.toString())) {
            return R.string.symptom_skin;
        } else if (category.equals(SymptomCategories.RESPIRATORY_SYTEM.toString())) {
            return R.string.symptom_respiratory_system;
        } else if (category.equals(SymptomCategories.GASTROINTESTINAL.toString())) {
            return R.string.symptom_gastrointestinal;
        } else if (category.equals(SymptomCategories.CIRCULATION.toString())) {
            return R.string.symptom_circulation;
        } else if (category.equals(SymptomCategories.HEADACHE.toString())) {
            return R.string.symptom_headache;
        } else {
            return R.string.symptom_other;
        }
    }

    public int getSymptomCategoryColoredPicture() {
        if (category.equals(SymptomCategories.SKIN.toString())) {
            return R.drawable.ic_symptom_skin;
        } else if (category.equals(SymptomCategories.RESPIRATORY_SYTEM.toString())) {
            return R.drawable.ic_symptom_respiratory_system;
        } else if (category.equals(SymptomCategories.GASTROINTESTINAL.toString())) {
            return R.drawable.ic_symptom_gastrointestinal;
        } else if (category.equals(SymptomCategories.CIRCULATION.toString())) {
            return R.drawable.ic_symptom_circulation;
        } else if (category.equals(SymptomCategories.HEADACHE.toString())) {
            return R.drawable.ic_symptom_headache;
        } else {
            return R.drawable.ic_symptom_other;
        }
    }

    public int getFrameForImageInAddingActivity() {
        if (category.equals(SymptomCategories.SKIN.toString())) {
            return R.id.img_symptom_skin;
        } else if (category.equals(SymptomCategories.RESPIRATORY_SYTEM.toString())) {
            return R.id.img_symptom_respiratory_system;
        } else if (category.equals(SymptomCategories.GASTROINTESTINAL.toString())) {
            return R.id.img_symtpom_gastrointestinal;
        } else if (category.equals(SymptomCategories.CIRCULATION.toString())) {
            return R.id.img_symptom_circulation;
        } else if (category.equals(SymptomCategories.HEADACHE.toString())) {
            return R.id.img_symptom_headache;
        } else {
            return R.id.img_symptom_other;
        }
    }

    public int getMedicineTookInSelectedLanguage() {
        if (tookMedicine == 0) {
            return R.string.export_took_no_medicine;
        } else {
            return R.string.export_took_medicine;
        }
    }

}
