package de.zissithebee.ernst.fragments.drawerFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import de.zissithebee.ernst.helper.NotificationHelper;
import de.zissithebee.ernst.R;
import de.zissithebee.ernst.fragments.pickerFragments.TimePickerFragment;
import de.zissithebee.ernst.helper.SharedPreferencesHelper;
import de.zissithebee.ernst.helper.ViewHandler;

public class SettingsFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private EditText etName;
    private Button btnBreakfastTime, btnLunchTime, btnDinnerTime, btnSnackTime, btnSave;
    private Switch switchRememberMe, switchRememberBreakfast, switchRememberLunch, switchRememberDinner, switchRememberSnack;

    private View view;

    private SharedPreferencesHelper sharedPreferencesHelper;
    private ViewHandler viewHandler;


    public SettingsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        sharedPreferencesHelper = new SharedPreferencesHelper(getActivity());
        btnSave = view.findViewById(R.id.btn_settings_save);
        btnSave.setOnClickListener(this);
        initializeEditText();
        initializeButtons();
        initializeSwitches();
        initializeSettingsFromSharedPrefs();
        return view;
    }

    private void initializeEditText() {
        etName = view.findViewById(R.id.et_name);
        etName.setText(sharedPreferencesHelper.getName());
    }

    private void initializeButtons() {
        btnBreakfastTime = view.findViewById(R.id.settings_btn_remember_breakfast_time);
        btnBreakfastTime.setText(sharedPreferencesHelper.getRememberMeBreakfastTime());
        btnBreakfastTime.setOnClickListener(this);

        btnLunchTime = view.findViewById(R.id.settings_btn_remember_lunch_time);
        btnLunchTime.setText(sharedPreferencesHelper.getRememberMeLunchTime());
        btnLunchTime.setOnClickListener(this);

        btnDinnerTime = view.findViewById(R.id.settings_btn_remember_dinner_time);
        btnDinnerTime.setText(sharedPreferencesHelper.getRememberMeDinnerTime());
        btnDinnerTime.setOnClickListener(this);

        btnSnackTime = view.findViewById(R.id.settings_btn_remember_snacks_time);
        btnSnackTime.setText(sharedPreferencesHelper.getRememberMeSnacksTime());
        btnSnackTime.setOnClickListener(this);
    }

    private void initializeSwitches() {
        switchRememberMe = view.findViewById(R.id.settings_switch_remember_me);
        switchRememberMe.setOnCheckedChangeListener(this);

        switchRememberBreakfast = view.findViewById(R.id.settings_switch_remember_breakfast);
        switchRememberBreakfast.setOnCheckedChangeListener(this);

        switchRememberLunch = view.findViewById(R.id.settings_switch_remember_lunch);
        switchRememberLunch.setOnCheckedChangeListener(this);

        switchRememberDinner = view.findViewById(R.id.settings_switch_remember_dinner);
        switchRememberDinner.setOnCheckedChangeListener(this);

        switchRememberSnack = view.findViewById(R.id.settings_switch_remember_snacks);
        switchRememberSnack.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btnSave) {
            saveSettings();
            startNewReminder();
            showToast();
        } else {
            TimePickerFragment timePickerFragment = new TimePickerFragment((Button) view);
            timePickerFragment.show(getActivity().getSupportFragmentManager(), "timePicker");
        }

    }

    private void startNewReminder() {
        NotificationHelper notificationHelper = new NotificationHelper(getContext());
        notificationHelper.createNotificiationChannel();
        notificationHelper.setReminder();
    }

    private void showToast() {
        Toast.makeText(getContext(), R.string.save_settings_successful, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        int id = compoundButton.getId();
        Button button;
        switch (id) {
            case R.id.settings_switch_remember_me:
                decideToShowOtherSettings(b);
                break;
            case R.id.settings_switch_remember_breakfast:
                button = btnBreakfastTime;
                decideToEnableButton(button, b);
                break;
            case R.id.settings_switch_remember_lunch:
                button = btnLunchTime;
                decideToEnableButton(button, b);
                break;
            case R.id.settings_switch_remember_dinner:
                button = btnDinnerTime;
                decideToEnableButton(button, b);
                break;
            case R.id.settings_switch_remember_snacks:
                button = btnSnackTime;
                decideToEnableButton(button, b);
                break;
        }

    }

    private void decideToEnableButton(Button btn, Boolean enable) {
        btn.setEnabled(enable);
    }

    //switches show the current state from the sharedpreferences
    private void initializeSettingsFromSharedPrefs() {
        boolean rememberMe = sharedPreferencesHelper.getRememberMe();
        switchRememberMe.setChecked(rememberMe);
        decideToShowOtherSettings(rememberMe);

        boolean rememberMeBreakfast = sharedPreferencesHelper.getRememberMeBreakfast();
        switchRememberBreakfast.setChecked(rememberMeBreakfast);
        decideToEnableButton(btnBreakfastTime, rememberMeBreakfast);

        boolean rememberMeLunch = sharedPreferencesHelper.getRememberMeLunch();
        switchRememberLunch.setChecked(rememberMeLunch);
        decideToEnableButton(btnLunchTime, rememberMeLunch);

        boolean rememberMeDinner = sharedPreferencesHelper.getRememberMeDinner();
        switchRememberDinner.setChecked(rememberMeDinner);
        decideToEnableButton(btnDinnerTime, rememberMeDinner);

        boolean rememberMeSnacks = sharedPreferencesHelper.getRememberMeSnacks();
        switchRememberSnack.setChecked(rememberMeSnacks);
        decideToEnableButton(btnSnackTime, rememberMeSnacks);
    }

    private void decideToShowOtherSettings(Boolean show) {
        LinearLayout linlay = view.findViewById(R.id.settings_lin_lay_for_remmebering);
        if (show) {
            linlay.setVisibility(View.VISIBLE);
        } else {
            linlay.setVisibility(View.GONE);
        }
    }

    private void saveSettings() {
        sharedPreferencesHelper.setName(etName.getText().toString());
        sharedPreferencesHelper.setRememberMe(switchRememberMe.isChecked());

        sharedPreferencesHelper.setRememberMeBreakfast(switchRememberBreakfast.isChecked());
        sharedPreferencesHelper.setRememberMeBreakfastTime(btnBreakfastTime.getText().toString());

        sharedPreferencesHelper.setRememberMeLunch(switchRememberLunch.isChecked());
        sharedPreferencesHelper.setRememberMeLunchTime(btnLunchTime.getText().toString());

        sharedPreferencesHelper.setRememberMeDinner(switchRememberDinner.isChecked());
        sharedPreferencesHelper.setRememberMeDinnerTime(btnDinnerTime.getText().toString());

        sharedPreferencesHelper.setRememberMeSnacks(switchRememberSnack.isChecked());
        sharedPreferencesHelper.setRememberMeSnacksTime(btnSnackTime.getText().toString());
    }
}
