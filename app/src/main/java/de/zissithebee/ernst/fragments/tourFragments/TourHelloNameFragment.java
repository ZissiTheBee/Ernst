package de.zissithebee.ernst.fragments.tourFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.helper.SharedPreferencesHelper;
import de.zissithebee.ernst.helper.ViewHandler;

public class TourHelloNameFragment extends Fragment {


    private View view;

    private List<TextView> tvs;

    private TextView tvHelloName, tvNameNotEntered, tvIHelp, tvImNot, tvDoctor;

    private ViewHandler viewHandler;

    private int durationToFadeNameNotEnterdViewIn = 1000;
    private final int DURATION_TO_FADE_VIEW_IN = 4000;
    private final int DURATION_TO_FADE_WHAT_I_AM_NOT_VIEW_IN = 4000;
    private final int DURATION_TO_FADE_SHOW_DOCTOR_VIEW_IN = 4000;


    public TourHelloNameFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tour_hello_name, container, false);
        viewHandler = new ViewHandler(getActivity());
        initializeTvs();
        fadeViewsIn();
        return view;
    }

    private void initializeTvs() {
        tvs = new ArrayList<>();
        tvHelloName = (TextView) view.findViewById(R.id.welcome_name);
        tvs.add(tvHelloName);
        tvNameNotEntered = (TextView) view.findViewById(R.id.welcome_name_not_entered);
        tvs.add(tvNameNotEntered);
        decideWhatToShow();
        tvIHelp = (TextView) view.findViewById(R.id.welcome_i_help);
        tvs.add(tvIHelp);
        tvImNot = (TextView) view.findViewById(R.id.welcome_what_i_am_not);
        tvs.add(tvImNot);
        tvDoctor = (TextView) view.findViewById(R.id.welcome_show_a_doctor);
        tvs.add(tvDoctor);

    }

    private void decideWhatToShow() {
        SharedPreferencesHelper sharedPreferencesHelper = new SharedPreferencesHelper(getActivity());
        String name = sharedPreferencesHelper.getName();
        if (name != null && name.length() != 0) {
            tvHelloName.setText(getString(R.string.welcome_name, name));
            durationToFadeNameNotEnterdViewIn = 0;
        } else {
            tvHelloName.setText(getString(R.string.welcome_again));
        }
    }

    private void fadeViewsIn() {
        if (durationToFadeNameNotEnterdViewIn != 0) {
            viewHandler.fadeTextIn(tvNameNotEntered, durationToFadeNameNotEnterdViewIn);
        }
        viewHandler.fadeTextIn(tvIHelp, durationToFadeNameNotEnterdViewIn + DURATION_TO_FADE_VIEW_IN);
        viewHandler.fadeTextIn(tvImNot, durationToFadeNameNotEnterdViewIn + DURATION_TO_FADE_VIEW_IN + DURATION_TO_FADE_WHAT_I_AM_NOT_VIEW_IN);
        viewHandler.fadeTextIn(tvDoctor, durationToFadeNameNotEnterdViewIn + DURATION_TO_FADE_VIEW_IN + DURATION_TO_FADE_WHAT_I_AM_NOT_VIEW_IN + DURATION_TO_FADE_SHOW_DOCTOR_VIEW_IN);
    }
}
