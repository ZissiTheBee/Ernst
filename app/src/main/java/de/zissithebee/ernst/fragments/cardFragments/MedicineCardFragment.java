package de.zissithebee.ernst.fragments.cardFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.zissithebee.ernst.AddingActivity;
import de.zissithebee.ernst.enums.ChooseAddingFragmentEnum;
import de.zissithebee.ernst.R;
import de.zissithebee.ernst.database.Entry;
import de.zissithebee.ernst.dialogs.DeleteDialog;
import de.zissithebee.ernst.enums.Keeper;
import de.zissithebee.ernst.enums.KeysForEntry;
import de.zissithebee.ernst.enums.KeysForMedicine;
import de.zissithebee.ernst.fragments.MainFragment;
import de.zissithebee.ernst.fragments.drawerFragments.DetailsFragment;

import static de.zissithebee.ernst.enums.Keeper.ID;
import static de.zissithebee.ernst.enums.Keeper.TIME;
import static de.zissithebee.ernst.enums.Keeper.TITLE;

public class MedicineCardFragment extends Fragment implements View.OnClickListener {

    private View view;
    private Entry entry;

    public MedicineCardFragment() {
    }

    public MedicineCardFragment(Entry entry) {
        this.entry = entry;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.card_medicine, container, false);
        fillViews();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageButton editMedicine = (ImageButton) view.findViewById(R.id.btn_edit_medicine);
        editMedicine.setOnClickListener(this);
        ImageButton removeMedicine = (ImageButton) view.findViewById(R.id.btn_remove_medicine);
        removeMedicine.setOnClickListener(this);
    }

    private void fillViews() {
        TextView tvCategory = view.findViewById(R.id.tv_medicine_category);
        tvCategory.setText(entry.getMedicineTook().getMedicine().getMedicineCategoryInSelectedLanguage());

        TextView tvName = view.findViewById(R.id.tv_medicine_name);
        tvName.setText(entry.getMedicineTook().getMedicine().getName());

        TextView tvDose = view.findViewById(R.id.tv_medicine_dose);
        tvDose.setText(entry.getMedicineTook().getDose());

        TextView tvTime = view.findViewById(R.id.tv_medicine_time);
        tvTime.setText(entry.getTime());

        ImageView image = (ImageView) view.findViewById(R.id.img_mc);
        image.setImageResource(entry.getMedicineTook().getMedicine().getMedicineCategoryImage());

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_remove_medicine:
                deleteEntry();
                break;

            case R.id.btn_edit_medicine:
                editEntry();
                break;

        }
    }

    private void deleteEntry() {
        List<Fragment> fragments = getFragments();
        DeleteDialog deleteDialog = new DeleteDialog(fragments);
        Bundle args = new Bundle();
        args.putInt(ID.toString(), entry.getId());
        args.putString(TIME.toString(), entry.getDate() + " " + entry.getTime());
        args.putString(TITLE.toString(), getString(entry.getCategoryInSlectedLanguage()));
        deleteDialog.setArguments(args);
        deleteDialog.show(getActivity().getSupportFragmentManager(), "dialogDelete");
    }

    private void editEntry() {
        Bundle entryToEdit = entryBundle();
        Intent intent = new Intent(getActivity(), AddingActivity.class);
        intent.putExtra(Keeper.BUNDLE.toString(), entryToEdit);
        intent.putExtra(ChooseAddingFragmentEnum.CHOOSE_ADDING_FRAGMENT.toString(), ChooseAddingFragmentEnum.ADD_MEDICINE.toString());
        startActivity(intent);
    }

    private List<Fragment> getFragments() {
        if (getArguments().getBoolean("Calendar")) {
            DetailsFragment detailsFragment = (DetailsFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.frame_layout);
            List<Fragment> thisFragment = new ArrayList();
            thisFragment.add(detailsFragment);
            return thisFragment;
        } else {
            MainFragment mainFragment = (MainFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.frame_layout);
            return mainFragment.getFragments();
        }
    }

    private Bundle entryBundle() {
        Bundle entryBundle = new Bundle();
        entryBundle.putInt(KeysForEntry.ID.toString(), entry.getId());
        entryBundle.putString(KeysForEntry.DATE.toString(), entry.getDate());
        entryBundle.putString(KeysForEntry.TIME.toString(), entry.getTime());
        entryBundle.putString(KeysForMedicine.NAME.toString(), entry.getMedicineTook().getMedicine().getName());
        entryBundle.putString(KeysForMedicine.DOSE.toString(), entry.getMedicineTook().getDose());
        entryBundle.putString(KeysForMedicine.CATEGORY.toString(), entry.getMedicineTook().getMedicine().getCategory());
        entryBundle.putInt(KeysForMedicine.CATEGORY_IMAGE.toString(), entry.getMedicineTook().getMedicine().getFrameForImageInAddingActivity());
        entryBundle.putInt(KeysForMedicine.ID_MEDICINE.toString(), entry.getMedicineTook().getMedicineId());
        entryBundle.putBoolean(Keeper.IS_UPDATE.toString(), true);

        return entryBundle;
    }
}
