package de.zissithebee.ernst.fragments.addingFragments;


import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.zissithebee.ernst.enums.EntryCategories;
import de.zissithebee.ernst.R;
import de.zissithebee.ernst.database.Entry;
import de.zissithebee.ernst.database.ErnstDatabase;
import de.zissithebee.ernst.database.Symptom;
import de.zissithebee.ernst.enums.Keeper;
import de.zissithebee.ernst.enums.KeysForEntry;
import de.zissithebee.ernst.enums.KeysForSymptom;
import de.zissithebee.ernst.enums.SymptomCategories;
import de.zissithebee.ernst.helper.ClickListenerHelper;
import de.zissithebee.ernst.helper.IdGetter;
import de.zissithebee.ernst.helper.ViewHandler;


public class AddSymptomFragment extends Fragment implements View.OnClickListener {


    private Button btn_symptom_begin_date, btn_symptom_begin_time, btn_symptom_end_date, btn_symptom_end_time;
    private int lastSelectedSymptomId;
    private String selectedSymptom;
    private View view;
    private ViewHandler viewHandler;
    private SeekBar sB_symptom_strength;
    private boolean isUpdateView = false;

    public AddSymptomFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_symptom, container, false);
        initialize();
        initializeButtons();
        setStepsForSeekBar();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            if (getArguments().getBoolean(Keeper.IS_UPDATE.toString())) {
                isUpdateView = true;
                fillViesWithArguments();
            }
        } catch (NullPointerException e) {
            //nothing to do here
        }
    }

    private void fillViesWithArguments() {
        EditText et_symptom_description = (EditText) view.findViewById(R.id.et_symptom_description);
        et_symptom_description.setText(getArguments().getString(KeysForSymptom.DESCRIPTION.toString()));
        Switch tookMedicineSwitch = (Switch) view.findViewById(R.id.symptom_took_medicine);
        if (getArguments().getInt(KeysForSymptom.TOOK_MEDICINE.toString()) == 0) {
            tookMedicineSwitch.setChecked(false);
        } else {
            tookMedicineSwitch.setChecked(true);
        }
        sB_symptom_strength.setProgress(getArguments().getInt(KeysForSymptom.STRENGTH.toString()));
        btn_symptom_begin_date.setText(getArguments().getString(KeysForEntry.DATE.toString()));
        btn_symptom_begin_time.setText(getArguments().getString(KeysForEntry.TIME.toString()));
        btn_symptom_end_date.setText(getArguments().getString(KeysForEntry.END_DATE.toString()));
        btn_symptom_end_time.setText(getArguments().getString(KeysForEntry.END_TIME.toString()));
        setUnselected();
        setSelected(getArguments().getInt(KeysForSymptom.CATEGORY_IMAGE.toString()));
    }

    private void initialize() {
        viewHandler = new ViewHandler(getActivity());
        IdGetter idGetter = new IdGetter();
        ClickListenerHelper clickListenerHelper = new ClickListenerHelper();
        int ids[] = idGetter.getSymptomImageIds();
        lastSelectedSymptomId = R.id.img_symptom_other;
        selectedSymptom = SymptomCategories.OTHER.toString();
        clickListenerHelper.setOnClickListenerImages(ids, view, this);
    }

    private void initializeButtons() {
        btn_symptom_begin_date = (Button) view.findViewById(R.id.btn_add_symptom_begin_date);
        btn_symptom_begin_date.setText(viewHandler.getCurrentDate());
        btn_symptom_begin_date.setOnClickListener(this);

        btn_symptom_begin_time = (Button) view.findViewById(R.id.btn_add_symptom_begin_time);
        btn_symptom_begin_time.setText(viewHandler.getCurrentTime());
        btn_symptom_begin_time.setOnClickListener(this);

        btn_symptom_end_date = (Button) view.findViewById(R.id.btn_add_symptom_end_date);
        btn_symptom_end_date.setText(viewHandler.getCurrentDate());
        btn_symptom_end_date.setOnClickListener(this);

        btn_symptom_end_time = (Button) view.findViewById(R.id.btn_add_symptom_end_time);
        btn_symptom_end_time.setText(viewHandler.getCurrentTime());
        btn_symptom_end_time.setOnClickListener(this);
    }

    private void setStepsForSeekBar() {
        sB_symptom_strength = (SeekBar) view.findViewById(R.id.sB_symptom_strength);
        sB_symptom_strength.setMax(9);
        sB_symptom_strength.setProgress(5);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_add_symptom_begin_date:
            case R.id.btn_add_symptom_end_date:
                viewHandler.showDatePickerFragment((Button) view.findViewById(id), this);
                break;
            case R.id.btn_add_symptom_begin_time:
            case R.id.btn_add_symptom_end_time:
                viewHandler.showTimePickerFrgament((Button) view.findViewById(id), this);
                break;
            case R.id.img_symptom_skin:
            case R.id.img_symptom_respiratory_system:
            case R.id.img_symtpom_gastrointestinal:
            case R.id.img_symptom_circulation:
            case R.id.img_symptom_headache:
            case R.id.img_symptom_other:
                setUnselected();
                setSelected(id);

        }
    }

    private void setUnselected() {
        ImageView selectedView = view.findViewById(lastSelectedSymptomId);
        switch (lastSelectedSymptomId) {
            case R.id.img_symptom_skin:
                selectedView.setImageResource(R.drawable.ic_symptom_skin_bw);
                break;
            case R.id.img_symptom_respiratory_system:
                selectedView.setImageResource(R.drawable.ic_symptom_respiratory_system_bw);
                break;
            case R.id.img_symtpom_gastrointestinal:
                selectedView.setImageResource(R.drawable.ic_symptom_gastrointestinal_bw);
                break;
            case R.id.img_symptom_circulation:
                selectedView.setImageResource(R.drawable.ic_symptom_circulation_bw);
                break;
            case R.id.img_symptom_headache:
                selectedView.setImageResource(R.drawable.ic_symptom_headache_bw);
                break;
            case R.id.img_symptom_other:
                selectedView.setImageResource(R.drawable.ic_symptom_other_bw);
                break;
        }
    }

    private void setSelected(int id) {
        lastSelectedSymptomId = id;
        ImageView selectedView = (ImageView) view.findViewById(id);
        switch (id) {
            case R.id.img_symptom_skin:
                selectedView.setImageResource(R.drawable.ic_symptom_skin);
                selectedSymptom = SymptomCategories.SKIN.toString();
                break;
            case R.id.img_symptom_respiratory_system:
                selectedView.setImageResource(R.drawable.ic_symptom_respiratory_system);
                selectedSymptom = SymptomCategories.RESPIRATORY_SYTEM.toString();
                break;
            case R.id.img_symtpom_gastrointestinal:
                selectedView.setImageResource(R.drawable.ic_symptom_gastrointestinal);
                selectedSymptom = SymptomCategories.GASTROINTESTINAL.toString();
                break;
            case R.id.img_symptom_circulation:
                selectedView.setImageResource(R.drawable.ic_symptom_circulation);
                selectedSymptom = SymptomCategories.CIRCULATION.toString();
                break;
            case R.id.img_symptom_headache:
                selectedView.setImageResource(R.drawable.ic_symptom_headache);
                selectedSymptom = SymptomCategories.HEADACHE.toString();
                break;
            case R.id.img_symptom_other:
                selectedView.setImageResource(R.drawable.ic_symptom_other);
                selectedSymptom = SymptomCategories.OTHER.toString();
                break;
        }
    }

    public void checkSave() {
        isEndDateBeforeStartDate();
        if (isEndDateBeforeStartDate()) {
            Toast.makeText(getContext(), getString(R.string.hint_end_date_before_start_date), Toast.LENGTH_LONG).show();
        } else if (selectedSymptom == null) {
            Toast.makeText(getContext(), getString(R.string.hint_select_symptom_category), Toast.LENGTH_LONG).show();
        } else {
            save();
            getActivity().finish();
        }
    }

    private boolean isEndDateBeforeStartDate() {
        String beginDateString = btn_symptom_begin_date.getText().toString();
        String endDateString = btn_symptom_end_date.getText().toString();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            Date beginDate = simpleDateFormat.parse(beginDateString);
            Date endDate = simpleDateFormat.parse(endDateString);
            if (endDate.before(beginDate)) {
                return true;
            } else if (beginDate.equals(endDate)) {
                return isEndTimeBeforeStartTime();
            }
        } catch (ParseException e) {
            Log.e("Error", e.toString());
        }
        return false;
    }

    private boolean isEndTimeBeforeStartTime() {
        String beginTimeString = btn_symptom_begin_time.getText().toString();
        String endTimeString = btn_symptom_end_time.getText().toString();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.GERMAN);
        try {
            Date beginTime = sdf.parse(beginTimeString);
            Date endTime = sdf.parse(endTimeString);
            if (endTime.before(beginTime)) {
                return true;
            } else if (endTime.equals(beginTime)) {
                return false;
            }
        } catch (ParseException e) {
            Log.e("Error", e.toString());
        }
        return false;
    }

    public void save() {
        EditText et_symptom_description = (EditText) view.findViewById(R.id.et_symptom_description);
        Switch tookMedicineSwitch = (Switch) view.findViewById(R.id.symptom_took_medicine);

        final String description = et_symptom_description.getText().toString();
        boolean tookMedicine = tookMedicineSwitch.isChecked();
        int took;
        if (tookMedicine) {
            took = 1;
        } else {
            took = 0;
        }
        final int forSave = took;
        final String selectedSymptomForSave = selectedSymptom;
        final int symptomStrenght = (sB_symptom_strength.getProgress());
        final String beginDate = btn_symptom_begin_date.getText().toString();
        final String beginTime = btn_symptom_begin_time.getText().toString();
        final String endDate = btn_symptom_end_date.getText().toString();
        final String endTime = btn_symptom_end_time.getText().toString();

        Entry entry = new Entry(beginDate, beginTime, endDate, endTime, EntryCategories.SYMPTOM.toString());

        if (isUpdateView) {
            entry.setId(getArguments().getInt(KeysForEntry.ID.toString()));
            updateEntry(entry, selectedSymptomForSave, description, symptomStrenght, forSave);
        } else {
            createNewEntry(entry, selectedSymptomForSave, description, symptomStrenght, forSave);
        }
    }

    private void updateEntry(final Entry entry, final String selectedSymptomForSave, final String description, final int symptomStrenght, final int forSave) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                ErnstDatabase.getInstance(getActivity())
                        .entryDao()
                        .updateEntry(entry);
                Symptom symptom = new Symptom((int) entry.getId(), selectedSymptomForSave, description, symptomStrenght, forSave);
                ErnstDatabase.getInstance(getActivity())
                        .symptomDao()
                        .updateSymptom(symptom);
            }
        }).start();
    }

    private void createNewEntry(final Entry entry, final String selectedSymptomForSave, final String description, final int symptomStrenght, final int forSave) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                long entryId = ErnstDatabase.getInstance(getActivity())
                        .entryDao()
                        .insertEntry(entry);
                Symptom symptom = new Symptom((int) entryId, selectedSymptomForSave, description, symptomStrenght, forSave);
                ErnstDatabase.getInstance(getActivity())
                        .symptomDao()
                        .insertSymptom(symptom);
            }
        }).start();
    }

}
