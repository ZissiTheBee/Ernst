package de.zissithebee.ernst.fragments.drawerFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.zissithebee.ernst.BuildConfig;
import de.zissithebee.ernst.R;

public class AboutFragment extends Fragment {


    public AboutFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        TextView tvVersionNumber = (TextView) view.findViewById(R.id.about_versionnumber);
        String versionName = BuildConfig.VERSION_NAME;
        tvVersionNumber.setText(getString(R.string.about_versionnumber, versionName));
        return view;
    }

}
