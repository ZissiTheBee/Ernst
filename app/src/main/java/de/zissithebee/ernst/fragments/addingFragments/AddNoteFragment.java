package de.zissithebee.ernst.fragments.addingFragments;


import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.database.Entry;
import de.zissithebee.ernst.database.ErnstDatabase;
import de.zissithebee.ernst.database.Note;
import de.zissithebee.ernst.enums.EntryCategories;
import de.zissithebee.ernst.enums.Keeper;
import de.zissithebee.ernst.enums.KeysForEntry;
import de.zissithebee.ernst.enums.KeysForNote;
import de.zissithebee.ernst.helper.ViewHandler;


public class AddNoteFragment extends Fragment implements View.OnClickListener {

    private View view;
    private ViewHandler viewHandler;
    private boolean isUpdateView = false;


    public AddNoteFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_note, container, false);
        viewHandler = new ViewHandler(getActivity());

        initializeViews();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            if (getArguments().getBoolean(Keeper.IS_UPDATE.toString())) {
                isUpdateView = true;
                fillViesWithArguments();
            }
        } catch (NullPointerException e) {
            //nothing to do here
        }
    }

    private void initializeViews() {
        Button btnAddNoteDate = (Button) view.findViewById(R.id.btn_add_note_date);
        btnAddNoteDate.setOnClickListener(this);
        btnAddNoteDate.setText(viewHandler.getCurrentDate());

        Button btnAddNoteTime = (Button) view.findViewById(R.id.btn_add_note_time);
        btnAddNoteTime.setOnClickListener(this);
        btnAddNoteTime.setText(viewHandler.getCurrentTime());
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_add_note_date:
                viewHandler.showDatePickerFragment((Button) view.findViewById(id), this);
                break;
            case R.id.btn_add_note_time:
                viewHandler.showTimePickerFrgament((Button) view.findViewById(id), this);
                break;
        }
    }

    public void checkSave() {
        final Button date = (Button) view.findViewById(R.id.btn_add_note_date);
        final Button time = (Button) view.findViewById(R.id.btn_add_note_time);
        final String dateForSave = date.getText().toString();
        final String timeForSave = time.getText().toString();

        EditText eTNote = (EditText) view.findViewById(R.id.et_note);
        final String noteText = eTNote.getText().toString();

        final Entry entry = new Entry(dateForSave, timeForSave, null, null, EntryCategories.NOTE.toString());

        if (isUpdateView) {
            updateEntry(entry, noteText);
        } else {
            createNewEntry(entry, noteText);
        }
        getActivity().finish();
    }

    private void updateEntry(final Entry entry, final String noteText) {
        final Note note = new Note(getArguments().getInt(KeysForEntry.ID.toString()), noteText);
        new Thread(new Runnable() {
            @Override
            public void run() {
                ErnstDatabase.getInstance(getActivity())
                        .noteDao()
                        .updateNote(note);
            }
        }).start();
    }

    private void createNewEntry(final Entry entry, final String noteText) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                long entryId = ErnstDatabase.getInstance(getActivity())
                        .entryDao()
                        .insertEntry(entry);
                Note note = new Note((int) entryId, noteText);
                ErnstDatabase.getInstance(getActivity())
                        .noteDao()
                        .insertNote(note);
            }
        }).start();
    }


    private void fillViesWithArguments() {
        Button dateButton = (Button) view.findViewById(R.id.btn_add_note_date);
        dateButton.setText(getArguments().getString(KeysForEntry.DATE.toString()));

        Button timeButton = (Button) view.findViewById(R.id.btn_add_note_time);
        timeButton.setText(getArguments().getString(KeysForEntry.TIME.toString()));

        EditText etNote = view.findViewById(R.id.et_note);
        etNote.setText(getArguments().getString(KeysForNote.NOTE.toString()));
    }
}
