package de.zissithebee.ernst.fragments.pickerFragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.Button;
import android.widget.TimePicker;

import java.util.Calendar;

import static android.text.format.DateFormat.is24HourFormat;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    private Button button;

    public TimePickerFragment() {
    }

    public TimePickerFragment(Button button) {
        this.button = button;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        return new TimePickerDialog(getActivity(), this, hour, minute, is24HourFormat(getActivity()));
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
        if (button != null) {
            String minuteString;
            String hourString;
            if (minute < 10) {
                minuteString = "0" + minute;
            } else {
                minuteString = Integer.toString(minute);
            }
            if (hour < 10) {
                hourString = "0" + hour;
            } else {
                hourString = Integer.toString(hour);
            }
            button.setText(hourString + ":" + minuteString);
        }
    }
}
