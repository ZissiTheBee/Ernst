package de.zissithebee.ernst.fragments.drawerFragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import de.zissithebee.ernst.BuildConfig;
import de.zissithebee.ernst.R;


public class FeedbackFragment extends Fragment implements View.OnClickListener {

    private final static char[] MAIL_CRYPTED = {154, 141, 145, 140, 139, 191, 145, 154, 139, 157, 154, 154, 140, 209, 155, 154};

    public FeedbackFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        Button buttonFeedback = (Button) view.findViewById(R.id.btn_feedback_now);
        buttonFeedback.setOnClickListener(this);
        return view;
    }

    private String getMailAddress() {
        String mail = "";
        for (char c : MAIL_CRYPTED) {
            mail += (char) (c ^ 0xff);
        }
        return mail;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_feedback_now) {
            sendFeedbackMail();
        }
    }


    private void sendFeedbackMail() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        String versionName = BuildConfig.VERSION_NAME;
        String uriText = "mailto:" + Uri.encode(getMailAddress()) + "?subject=" +
                Uri.encode(getString(R.string.feedback_mail_headline, versionName));
        Uri uri = Uri.parse(uriText);
        intent.setData(uri);
        startActivity(Intent.createChooser(intent, getString(R.string.nav_help)));

    }
}
