package de.zissithebee.ernst.fragments.drawerFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.HashSet;

import de.zissithebee.ernst.AddingActivity;
import de.zissithebee.ernst.enums.FoodCategories;
import de.zissithebee.ernst.enums.Keeper;
import de.zissithebee.ernst.enums.KeysForDish;
import de.zissithebee.ernst.enums.MedicineCategories;
import de.zissithebee.ernst.enums.SymptomCategories;
import de.zissithebee.ernst.R;
import de.zissithebee.ernst.asynctasks.OverviewFragmentTask;
import de.zissithebee.ernst.helper.ClickListenerHelper;
import de.zissithebee.ernst.helper.IdGetter;

import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.ADD_FOOD;
import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.CHOOSE_ADDING_FRAGMENT;


public class OverviewFragment extends Fragment implements View.OnClickListener {

    private View view;
    private ViewGroup container;
    private ProgressBar overviewProgress;

    public OverviewFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.container = container;
        view = inflater.inflate(R.layout.fragment_overview, container, false);
        overviewProgress = view.findViewById(R.id.progress_overview);
        IdGetter idGetter = new IdGetter();
        int viewIds[] = idGetter.getOverviewFoodImageIds();
        ClickListenerHelper clickListenerHelper = new ClickListenerHelper();
        clickListenerHelper.setOnClickListenerImages(viewIds, view, this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        executeTask();
    }


    public void executeTask() {
        overviewProgress.setVisibility(View.VISIBLE);
        new OverviewFragmentTask().execute(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    //updates the dish card
    public void updateFoodView(HashSet categories) {
        if (categories.contains(FoodCategories.BREAKFAST.toString())) {
            ImageView imgBreakfast = (ImageView) view.findViewById(R.id.img_breakfast);
            imgBreakfast.setImageResource(R.drawable.ic_food_breakfast);
        }
        if (categories.contains(FoodCategories.LUNCH.toString())) {
            ImageView imgLunch = (ImageView) view.findViewById(R.id.img_lunch);
            imgLunch.setImageResource(R.drawable.ic_food_lunch);
        }
        if (categories.contains(FoodCategories.DINNER.toString())) {
            ImageView imgDinner = (ImageView) view.findViewById(R.id.img_dinner);
            imgDinner.setImageResource(R.drawable.ic_food_dinner);
        }
        if (categories.contains(FoodCategories.SNACK.toString())) {
            ImageView imgSnack = (ImageView) view.findViewById(R.id.img_snacks);
            imgSnack.setImageResource(R.drawable.ic_food_snack);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        Intent intent = new Intent(getActivity(), AddingActivity.class);
        intent.putExtra(CHOOSE_ADDING_FRAGMENT.toString(), ADD_FOOD.toString());
        Bundle args = new Bundle();
        switch (id) {
            case R.id.img_breakfast:
                args.putString(KeysForDish.CATEGORY.toString(), FoodCategories.BREAKFAST.toString());
                break;
            case R.id.img_lunch:
                args.putString(KeysForDish.CATEGORY.toString(), FoodCategories.LUNCH.toString());
                break;
            case R.id.img_dinner:
                args.putString(KeysForDish.CATEGORY.toString(), FoodCategories.DINNER.toString());
                break;
            case R.id.img_snacks:
                args.putString(KeysForDish.CATEGORY.toString(), FoodCategories.SNACK.toString());
                break;
        }
        intent.putExtra(Keeper.BUNDLE.toString(), args);
        startActivity(intent);
    }

    public void updateSymptomView(HashSet<String> categories) {
        try {
            int i = 0;
            for (String category : categories) {
                if (i == 4) {
                    break;
                }
                int drawable = 0;
                String description = "";
                if (category.equals(SymptomCategories.SKIN.toString())) {
                    drawable = R.drawable.ic_symptom_skin;
                    description = getResources().getString(R.string.symptom_skin);
                } else if (category.equals(SymptomCategories.RESPIRATORY_SYTEM.toString())) {
                    drawable = R.drawable.ic_symptom_respiratory_system;
                    description = getResources().getString(R.string.symptom_respiratory_system);
                } else if (category.equals(SymptomCategories.CIRCULATION.toString())) {
                    drawable = R.drawable.ic_symptom_circulation;
                    description = getResources().getString(R.string.symptom_circulation);
                } else if (category.equals(SymptomCategories.GASTROINTESTINAL.toString())) {
                    drawable = R.drawable.ic_symptom_gastrointestinal;
                    description = getResources().getString(R.string.symptom_gastrointestinal);
                } else if (category.equals(SymptomCategories.HEADACHE.toString())) {
                    drawable = R.drawable.ic_symptom_headache;
                    description = getResources().getString(R.string.symptom_headache);
                } else if (category.equals(SymptomCategories.OTHER.toString())) {
                    drawable = R.drawable.ic_symptom_other;
                    description = getResources().getString(R.string.symptom_other);
                }
                ImageView symptomImage;
                TextView symptomText;
                if (i == 0) {
                    symptomImage = (ImageView) view.findViewById(R.id.img_symptom1);
                    symptomText = (TextView) view.findViewById(R.id.text_symptom1);
                    symptomImage.setImageResource(drawable);
                    symptomText.setText(description);
                } else if (i == 1) {
                    symptomImage = (ImageView) view.findViewById(R.id.img_symptom2);
                    symptomText = (TextView) view.findViewById(R.id.text_symptom2);
                    symptomImage.setImageResource(drawable);
                    symptomText.setText(description);
                } else if (i == 2) {
                    symptomImage = (ImageView) view.findViewById(R.id.img_symptom3);
                    symptomText = (TextView) view.findViewById(R.id.text_symptom3);
                    symptomImage.setImageResource(drawable);
                    symptomText.setText(description);
                } else if (i == 3) {
                    symptomImage = (ImageView) view.findViewById(R.id.img_symptom4);
                    symptomText = (TextView) view.findViewById(R.id.text_symptom4);
                    symptomImage.setImageResource(drawable);
                    symptomText.setText(description);
                }
                i++;
            }
        } catch (Exception e) {
            //nothing to do here
        }
    }

    public void updateMedicineView(HashSet<String> categories) {
        int i = 0;
        for (String category : categories) {
            if (i == 4) {
                break;
            }
            int drawable = 0;
            String description = "";
            if (category.equals(MedicineCategories.ANTIHISTAMINE.toString())) {
                drawable = R.drawable.ic_medicine_antihistamine;
                description = getResources().getString(R.string.medicine_antihistamine);
            } else if (category.equals(MedicineCategories.CORTISONE.toString())) {
                drawable = R.drawable.ic_medicine_cortisone;
                description = getResources().getString(R.string.medicine_cortisone);
            } else if (category.equals(MedicineCategories.PAINKILLER.toString())) {
                drawable = R.drawable.ic_medicine_painkiller;
                description = getResources().getString(R.string.medicine_painkiller);
            } else if (category.equals(MedicineCategories.SKIN_CREAM.toString())) {
                drawable = R.drawable.ic_medicine_skin_cream;
                description = getResources().getString(R.string.medicine_skin_creame);
            } else if (category.equals(MedicineCategories.GASTROINTESTINAL.toString())) {
                drawable = R.drawable.ic_medicine_gastrointestinal_medicine;
                description = getResources().getString(R.string.medicine_gastrointestinal_medecine);
            } else if (category.equals(MedicineCategories.ASTHMA.toString())) {
                drawable = R.drawable.ic_medicine_asthma_medicine;
                description = getResources().getString(R.string.medicine_asthma_medicine);
            } else if (category.equals(MedicineCategories.ANTIBIOTIC.toString())) {
                drawable = R.drawable.ic_medicine_antibiotic;
                description = getResources().getString(R.string.medicine_antibiotic);
            } else if (category.equals(MedicineCategories.COLD_MED.toString())) {
                drawable = R.drawable.ic_medicine_cold;
                description = getResources().getString(R.string.medicine_cold_medicine);
            } else {
                drawable = R.drawable.ic_medicine_other;
                description = getResources().getString(R.string.medicine_other);
            }
            ImageView symptomImage;
            TextView symptomText;
            if (i == 0) {
                symptomImage = (ImageView) view.findViewById(R.id.img_medicine1);
                symptomText = (TextView) view.findViewById(R.id.text_med1);
                symptomImage.setImageResource(drawable);
                symptomText.setText(description);
            } else if (i == 1) {
                symptomImage = (ImageView) view.findViewById(R.id.img_medicine2);
                symptomText = (TextView) view.findViewById(R.id.text_med2);
                symptomImage.setImageResource(drawable);
                symptomText.setText(description);
            } else if (i == 2) {
                symptomImage = (ImageView) view.findViewById(R.id.img_medicine3);
                symptomText = (TextView) view.findViewById(R.id.text_med3);
                symptomImage.setImageResource(drawable);
                symptomText.setText(description);
            } else if (i == 3) {
                symptomImage = (ImageView) view.findViewById(R.id.img_medicine4);
                symptomText = (TextView) view.findViewById(R.id.text_med4);
                symptomImage.setImageResource(drawable);
                symptomText.setText(description);
            }
            i++;
        }
    }

    public void updateNoteView(String note) {
        if (note != null) {
            TextView tvNote = (TextView) view.findViewById(R.id.tv_note);
            tvNote.setText(note);
        }
        overviewProgress.setVisibility(View.GONE);
    }
}
