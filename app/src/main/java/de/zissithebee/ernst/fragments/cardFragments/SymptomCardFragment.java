package de.zissithebee.ernst.fragments.cardFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.zissithebee.ernst.AddingActivity;
import de.zissithebee.ernst.enums.ChooseAddingFragmentEnum;
import de.zissithebee.ernst.dialogs.DeleteDialog;
import de.zissithebee.ernst.enums.Keeper;
import de.zissithebee.ernst.enums.KeysForEntry;
import de.zissithebee.ernst.enums.KeysForSymptom;
import de.zissithebee.ernst.R;
import de.zissithebee.ernst.database.Entry;
import de.zissithebee.ernst.fragments.MainFragment;
import de.zissithebee.ernst.fragments.drawerFragments.DetailsFragment;

import static de.zissithebee.ernst.enums.Keeper.ID;
import static de.zissithebee.ernst.enums.Keeper.TIME;
import static de.zissithebee.ernst.enums.Keeper.TITLE;

public class SymptomCardFragment extends Fragment implements View.OnClickListener {

    private View view;
    private LayoutInflater inflater;
    private Entry entry;

    public SymptomCardFragment() {
    }

    public SymptomCardFragment(Entry entry) {
        this.entry = entry;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.card_symptom, container, false);
        this.inflater = inflater;
        fillTVs(entry);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageButton removeButton = (ImageButton) view.findViewById(R.id.btn_remove_symptom);
        removeButton.setOnClickListener(this);
        ImageButton editButton = (ImageButton) view.findViewById(R.id.btn_edit_symptom);
        editButton.setOnClickListener(this);
    }

    public void fillTVs(Entry entry) {
        TextView title = (TextView) view.findViewById(R.id.tv_sc_title);
        title.setText(entry.getSymptom().getSymptomCategoryInLanguage());
        ImageView image = (ImageView) view.findViewById(R.id.img_sc);
        image.setImageResource(entry.getSymptom().getSymptomCategoryColoredPicture());
        TextView tookMed = (TextView) view.findViewById(R.id.tv_sc_took_medicine);
        decideMedicineTook(entry.getSymptom().getTookMedicine(), tookMed);
        TextView description = (TextView) view.findViewById(R.id.tv_sc_description);
        description.setText(entry.getSymptom().getDescription());
        TextView strength = (TextView) view.findViewById(R.id.tv_sc_strength);
        strength.setText(Integer.toString(entry.getSymptom().getStrength()));
        TextView begin = (TextView) view.findViewById(R.id.tv_sc_begin);

        begin.setText(entry.getDate() + " " + entry.getTime());
        TextView end = (TextView) view.findViewById(R.id.tv_sc_end);
        end.setText(entry.getEndDate() + " " + entry.getEndTime());
    }

    private void decideMedicineTook(int tookMedicine, TextView tookMedTv) {
        if (tookMedicine == 0) {
            tookMedTv.setText(getActivity().getResources().getString(R.string.no));
        } else if (tookMedicine == 1) {
            tookMedTv.setText(getActivity().getResources().getString(R.string.yes));
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_remove_symptom:
                deleteEntry();
                break;

            case R.id.btn_edit_symptom:
                editEntry();
                break;
        }
    }

    private void deleteEntry() {
        DeleteDialog deleteDialog = new DeleteDialog(getFragments());
        Bundle args = new Bundle();
        args.putInt(ID.toString(), entry.getId());
        args.putString(TIME.toString(), entry.getDate() + " " + entry.getTime());
        args.putString(TITLE.toString(), getString(entry.getCategoryInSlectedLanguage()));
        deleteDialog.setArguments(args);
        deleteDialog.show(getActivity().getSupportFragmentManager(), "dialogDelete");
    }

    private void editEntry() {
        Bundle entryToEdit = entryBundle();
        Intent intent = new Intent(getActivity(), AddingActivity.class);
        intent.putExtra(Keeper.BUNDLE.toString(), entryToEdit);
        intent.putExtra(ChooseAddingFragmentEnum.CHOOSE_ADDING_FRAGMENT.toString(), ChooseAddingFragmentEnum.ADD_SYMPTOM.toString());
        startActivity(intent);
    }

    private List<Fragment> getFragments() {
        if (getArguments().getBoolean("Calendar")) {
            DetailsFragment detailsFragment = (DetailsFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.frame_layout);
            List<Fragment> thisFragment = new ArrayList();
            thisFragment.add(detailsFragment);
            return thisFragment;
        } else {
            MainFragment mainFragment = (MainFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.frame_layout);
            return mainFragment.getFragments();
        }
    }

    private Bundle entryBundle() {
        Bundle entryBundle = new Bundle();
        entryBundle.putInt(KeysForEntry.ID.toString(), entry.getId());
        entryBundle.putString(KeysForEntry.DATE.toString(), entry.getDate());
        entryBundle.putString(KeysForEntry.TIME.toString(), entry.getTime());
        entryBundle.putString(KeysForEntry.END_DATE.toString(), entry.getEndDate());
        entryBundle.putString(KeysForEntry.END_TIME.toString(), entry.getEndTime());
        entryBundle.putString(KeysForSymptom.CATEGORY.toString(), entry.getSymptom().getCategory());
        entryBundle.putString(KeysForSymptom.DESCRIPTION.toString(), entry.getSymptom().getDescription());
        entryBundle.putInt(KeysForSymptom.STRENGTH.toString(), entry.getSymptom().getStrength());
        entryBundle.putInt(KeysForSymptom.TOOK_MEDICINE.toString(), entry.getSymptom().getTookMedicine());
        entryBundle.putInt(KeysForSymptom.CATEGORY_IMAGE.toString(), entry.getSymptom().getFrameForImageInAddingActivity());
        entryBundle.putBoolean(Keeper.IS_UPDATE.toString(), true);
        return entryBundle;
    }
}
