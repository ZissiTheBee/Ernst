package de.zissithebee.ernst.fragments.tourFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.helper.ViewHandler;

public class TourWhatEntriesToDoFragment extends Fragment {

    private View view;

    private ViewHandler viewHandler;

    private TextView tvWhatToEnter, tvEntryCategories, tvHowToMakeEasy;

    private final int DURATION_TO_SHOW_ENTRY_CATEGORIES_TEXT = 4000;
    private final int DURATION_TO_SHOW_HOW_TO_MAKE_IT_EASY_TEXT = 5000;

    private List<TextView> tvs;

    public TourWhatEntriesToDoFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tour_what_entries_to_do, container, false);
        viewHandler = new ViewHandler(getActivity());
        initializeTvs();
        fadeViewsIn();
        return view;
    }

    private void initializeTvs() {
        tvs = new ArrayList<>();
        tvWhatToEnter = (TextView) view.findViewById(R.id.welcome_what_to_enter);
        tvs.add(tvWhatToEnter);
        tvEntryCategories = (TextView) view.findViewById(R.id.welcome_entry_categories);
        tvs.add(tvEntryCategories);
        tvHowToMakeEasy = (TextView) view.findViewById(R.id.welcome_easy);
        tvs.add(tvHowToMakeEasy);
    }


    private void fadeViewsIn() {
        viewHandler.fadeTextIn(tvEntryCategories, DURATION_TO_SHOW_ENTRY_CATEGORIES_TEXT);
        viewHandler.fadeTextIn(tvHowToMakeEasy, DURATION_TO_SHOW_ENTRY_CATEGORIES_TEXT + DURATION_TO_SHOW_HOW_TO_MAKE_IT_EASY_TEXT);
    }

}
