package de.zissithebee.ernst.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import de.zissithebee.ernst.R;

public class SharedPreferencesHelper {

    private SharedPreferences sharedPreferences;

    private Context context;

    private static final String IS_FIRST_START = "isFirstStart";
    private static final String NAME = "name";
    private static final String REMEMBER_ME = "rememberMe";
    private static final String REMEBER_ME_BREAKFAST = "rememberMeBreakfast";
    private static final String REMEBER_ME_BREAKFAST_TIME = "rememberMeBreakfastTime";
    private static final String REMEBER_ME_LUNCH = "rememberMeLunch";
    private static final String REMEBER_ME_LUNCH_TIME = "rememberMeLunchTime";
    private static final String REMEBER_ME_DINNER = "rememberMeDinner";
    private static final String REMEBER_ME_DINNER_TIME = "rememberMeDinnerTime";
    private static final String REMEBER_ME_SNACKS = "rememberMeSnacks";
    private static final String REMEBER_ME_SNACKS_TIME = "rememberMeSnacksTime";

    public SharedPreferencesHelper(Context context) {
        this.context = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setIsFirstStart(boolean isFirstStart) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_FIRST_START, isFirstStart);
        editor.commit();
    }

    public boolean getIsFirstStart() {
        return sharedPreferences.getBoolean(IS_FIRST_START, true);
    }

    public void setName(String name) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(NAME, name);
        editor.commit();
    }

    public String getName() {
        return sharedPreferences.getString(NAME, "");
    }

    public void setRememberMe(boolean rememberMe) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(REMEMBER_ME, rememberMe);
        editor.commit();
    }

    public boolean getRememberMe() {
        return sharedPreferences.getBoolean(REMEMBER_ME, true);
    }

    public void setRememberMeBreakfast(Boolean rememberMeBreakfast) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(REMEBER_ME_BREAKFAST, rememberMeBreakfast);
        editor.commit();
    }

    public Boolean getRememberMeBreakfast() {
        return sharedPreferences.getBoolean(REMEBER_ME_BREAKFAST, true);
    }

    public void setRememberMeBreakfastTime(String rememberMeBreakfastTime) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(REMEBER_ME_BREAKFAST_TIME, rememberMeBreakfastTime);
        editor.commit();
    }

    public String getRememberMeBreakfastTime() {
        return sharedPreferences.getString(REMEBER_ME_BREAKFAST_TIME, context.getResources().getString(R.string.welcome_time_to_remember_breakfast));
    }

    public void setRememberMeLunch(Boolean rememberMeLunch) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(REMEBER_ME_LUNCH, rememberMeLunch);
        editor.commit();
    }

    public Boolean getRememberMeLunch() {
        return sharedPreferences.getBoolean(REMEBER_ME_LUNCH, true);
    }

    public void setRememberMeLunchTime(String rememberMeLunchtTime) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(REMEBER_ME_LUNCH_TIME, rememberMeLunchtTime);
        editor.commit();
    }

    public String getRememberMeLunchTime() {
        return sharedPreferences.getString(REMEBER_ME_LUNCH_TIME, context.getResources().getString(R.string.welcome_time_to_remember_lunch));
    }

    public void setRememberMeDinner(Boolean rememberMeDinner) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(REMEBER_ME_DINNER, rememberMeDinner);
        editor.commit();
    }

    public Boolean getRememberMeDinner() {
        return sharedPreferences.getBoolean(REMEBER_ME_DINNER, true);
    }

    public void setRememberMeDinnerTime(String rememberMeDinnerTime) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(REMEBER_ME_DINNER_TIME, rememberMeDinnerTime);
        editor.commit();
    }

    public String getRememberMeDinnerTime() {
        return sharedPreferences.getString(REMEBER_ME_DINNER_TIME, context.getResources().getString(R.string.welcome_time_to_remember_dinner));
    }

    public void setRememberMeSnacks(Boolean rememberMeSnacks) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(REMEBER_ME_SNACKS, rememberMeSnacks);
        editor.commit();
    }

    public Boolean getRememberMeSnacks() {
        return sharedPreferences.getBoolean(REMEBER_ME_SNACKS, false);
    }

    public void setRememberMeSnacksTime(String rememberMeSnacksTimeTime) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(REMEBER_ME_SNACKS_TIME, rememberMeSnacksTimeTime);
        editor.commit();
    }

    public String getRememberMeSnacksTime() {
        return sharedPreferences.getString(REMEBER_ME_SNACKS_TIME, context.getResources().getString(R.string.welcome_time_to_remember_snacks));
    }

}
