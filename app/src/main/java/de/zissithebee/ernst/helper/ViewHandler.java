package de.zissithebee.ernst.helper;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.fragments.pickerFragments.DatePickerFragment;
import de.zissithebee.ernst.fragments.pickerFragments.TimePickerFragment;

public class ViewHandler {

    Context context;

    public ViewHandler(Context context) {
        this.context = context;
    }

    public void setTextInToolbar(String text, ActionBar actionBar, Boolean home) {
        TextView tv = new TextView(context);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        Typeface typeface = ResourcesCompat.getFont(context, R.font.amatic_sc_bold);
        tv.setTypeface(typeface);
        tv.setLayoutParams(lp);
        tv.setText(text);
        tv.setTextSize(30);
        tv.setMaxLines(1);
        tv.setTextColor(Color.WHITE);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(tv);
        if (home) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }

    public String getCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);
        Date date = new Date();
        return sdf.format(date);
    }

    public String getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.GERMAN);
        Date date = new Date();
        return sdf.format(date);
    }

    public void showDatePickerFragment(Button button, Fragment fragment) {
        DialogFragment dialogFragment = new DatePickerFragment(button);
        dialogFragment.show(fragment.getActivity().getSupportFragmentManager(), "datePicker");
    }

    public void showTimePickerFrgament(Button button, Fragment fragment) {
        TimePickerFragment timePickerFragment = new TimePickerFragment(button);
        timePickerFragment.show(fragment.getActivity().getSupportFragmentManager(), "timePicker");
    }

    public void fadeTextIn(final TextView tv, final int delay) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tv.setVisibility(View.VISIBLE);
                tv.setAlpha(0);
                tv.animate().setDuration(500).alpha(1);
            }
        }, delay);
    }
}
