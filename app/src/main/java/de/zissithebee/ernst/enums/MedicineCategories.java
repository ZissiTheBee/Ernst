package de.zissithebee.ernst.enums;

public enum MedicineCategories {
    ANTIHISTAMINE, CORTISONE, PAINKILLER, SKIN_CREAM, GASTROINTESTINAL, ASTHMA, ANTIBIOTIC, COLD_MED, OTHER
}
