package de.zissithebee.ernst.enums;

public enum KeysForSymptom {
    CATEGORY, DESCRIPTION, STRENGTH, TOOK_MEDICINE, CATEGORY_IMAGE
}
