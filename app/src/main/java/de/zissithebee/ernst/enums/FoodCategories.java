package de.zissithebee.ernst.enums;

public enum FoodCategories {
    BREAKFAST, LUNCH, DINNER, SNACK
}
