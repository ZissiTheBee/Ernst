package de.zissithebee.ernst.enums;

public enum KeysForDish {
    NAME, CATEGORY, MAIN_INGREDIENTS, INGREDIENTS, TIMES_EATEN, ID_DISH, ID_DISH_EATEN, CATEGORY_IMAGE
}
