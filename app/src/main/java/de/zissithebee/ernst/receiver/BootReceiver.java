package de.zissithebee.ernst.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import de.zissithebee.ernst.helper.NotificationHelper;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationHelper notificationHelper = new NotificationHelper(context);
        notificationHelper.createNotificiationChannel();
        notificationHelper.setReminder();
    }
}
