package de.zissithebee.ernst.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import de.zissithebee.ernst.helper.NotificationHelper;

import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.CHOOSE_ADDING_FRAGMENT;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationHelper notificationHelper = new NotificationHelper(context);
        String categoty = intent.getStringExtra(CHOOSE_ADDING_FRAGMENT.toString());
        notificationHelper.createNotification(categoty);
    }
}
