package de.zissithebee.ernst;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import de.zissithebee.ernst.fragments.tourFragments.TourRememberMeSettingsFragment;
import de.zissithebee.ernst.fragments.tourFragments.TourEnterYourNameFragment;
import de.zissithebee.ernst.fragments.tourFragments.TourHelloNameFragment;
import de.zissithebee.ernst.fragments.tourFragments.TourRememberMeFragment;
import de.zissithebee.ernst.fragments.tourFragments.TourStartNowFragment;
import de.zissithebee.ernst.fragments.tourFragments.TourWelcomeFragment;
import de.zissithebee.ernst.fragments.tourFragments.TourWhatEntriesToDoFragment;
import de.zissithebee.ernst.helper.SharedPreferencesHelper;
import de.zissithebee.ernst.helper.ViewHandler;

public class TourActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonNext, buttonSkip;

    private int screenNumber;

    private final int DURATION_WELCOME_TO_ERNST = 12000;

    private final int SCREEN_NUMBER_FOR_WELCOME_TO_ERNST_FRAGMENT = 1;
    private final int SCREEN_NUMBER_FOR_ENTER_YOUR_NAME_FRAGMENT = 2;
    private final int SCREEN_NUMBER_FOR_HELLO_ENTERED_NAME_FRAGMEN = 3;
    private final int SCREEN_NUMBER_FOR_WHAT_TO_ENTER_FRAGMENT = 4;
    private final int SCREEN_NUMBER_FOR_REMEMBER_ME_FRAGMENT = 5;
    private final int SCREEN_NUMBER_FOR_REMEMBER_SETTINGS_FRAGMENT = 6;
    private final int SCREEN_NUMBER_FOR_START_FRAGMNET = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        setContentView(R.layout.activity_tour);
        setOnClickListenerToButton();
        startFirstFragment();
    }

    private void setFullScreen() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
    }

    private void startFirstFragment() {
        replaceFragment(new TourWelcomeFragment());
        screenNumber = 1;
        fadeButtonIn(buttonNext, DURATION_WELCOME_TO_ERNST);
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_tour, fragment);
        fragmentTransaction.commit();
    }

    private void showEnterYourNameFragment() {
        replaceFragment(new TourEnterYourNameFragment());
        screenNumber++;
        buttonNext.setEnabled(false);
        fadeButtonIn(buttonNext, 1000);
    }

    private void setOnClickListenerToButton() {

        ViewHandler viewHandler = new ViewHandler(this);

        buttonNext = findViewById(R.id.btn_next_welcome);
        buttonNext.setOnClickListener(this);

        buttonSkip = findViewById(R.id.btn_skip_welcome);
        buttonSkip.setOnClickListener(this);
    }

    private void fadeButtonIn(final Button button, final int delay) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                button.setEnabled(true);
            }
        }, delay);
    }

    private void showWelcomeWithEnteredName() {
        replaceFragment(new TourHelloNameFragment());
        screenNumber++;
        buttonNext.setEnabled(false);
        fadeButtonIn(buttonNext, 16000);
    }

    private void showWhatToEnterFragment() {
        replaceFragment(new TourWhatEntriesToDoFragment());
        screenNumber++;
        buttonNext.setEnabled(false);
        fadeButtonIn(buttonNext, 12000);
    }

    private void showRememberMeFragment() {
        replaceFragment(new TourRememberMeFragment());
        screenNumber++;
        buttonNext.setEnabled(false);
        fadeButtonIn(buttonNext, 8000);
    }

    private void showStartNowFragment() {
        replaceFragment(new TourStartNowFragment());
        screenNumber++;
        buttonNext.setEnabled(false);
        buttonNext.setText(getString(R.string.welcome_btn_lets_start));
        fadeButtonIn(buttonNext, 8000);
    }

    private void showRememberMeSettingsFragment() {
        replaceFragment(new TourRememberMeSettingsFragment());
        screenNumber++;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_skip_welcome) {
            Intent intent = new Intent(this, MainActivity.class);
            saveTourFinished();
            startActivity(intent);
        } else {

            switch (screenNumber) {
                case SCREEN_NUMBER_FOR_WELCOME_TO_ERNST_FRAGMENT:
                    showEnterYourNameFragment();
                    break;
                case SCREEN_NUMBER_FOR_ENTER_YOUR_NAME_FRAGMENT:
                    saveName();
                    showWelcomeWithEnteredName();
                    break;
                case SCREEN_NUMBER_FOR_HELLO_ENTERED_NAME_FRAGMEN:
                    showWhatToEnterFragment();
                    break;
                case SCREEN_NUMBER_FOR_WHAT_TO_ENTER_FRAGMENT:
                    showRememberMeFragment();
                    break;
                case SCREEN_NUMBER_FOR_REMEMBER_ME_FRAGMENT:
                    showRememberMeSettingsFragment();
                    break;
                case SCREEN_NUMBER_FOR_REMEMBER_SETTINGS_FRAGMENT:
                    saveSettings();
                    showStartNowFragment();
                    break;
                case SCREEN_NUMBER_FOR_START_FRAGMNET:
                    saveTourFinished();
                    goToStart();
                    break;
            }
        }
    }

    private void saveTourFinished() {
        SharedPreferencesHelper sharedPreferencesHelper = new SharedPreferencesHelper(this);
        sharedPreferencesHelper.setIsFirstStart(false);
    }

    private void goToStart() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void saveName() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        TourEnterYourNameFragment tourEnterYourNameFragment = (TourEnterYourNameFragment) fragmentManager.findFragmentById(R.id.frame_layout_tour);
        tourEnterYourNameFragment.saveName();
    }

    private void saveSettings() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        TourRememberMeSettingsFragment tourRememberMeSettingsFragment = (TourRememberMeSettingsFragment) fragmentManager.findFragmentById(R.id.frame_layout_tour);
        tourRememberMeSettingsFragment.saveSettings();
        tourRememberMeSettingsFragment.startReminder();
    }
}
