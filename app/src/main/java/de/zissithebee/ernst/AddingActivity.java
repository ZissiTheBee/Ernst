package de.zissithebee.ernst;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import de.zissithebee.ernst.dialogs.CancelDialog;
import de.zissithebee.ernst.enums.Keeper;
import de.zissithebee.ernst.fragments.addingFragments.AddFoodFragment;
import de.zissithebee.ernst.fragments.addingFragments.AddMedicineFragment;
import de.zissithebee.ernst.fragments.addingFragments.AddNoteFragment;
import de.zissithebee.ernst.fragments.addingFragments.AddSymptomFragment;
import de.zissithebee.ernst.helper.ViewHandler;

import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.ADD_FOOD;
import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.ADD_MEDICINE;
import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.ADD_NOTE;
import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.ADD_SYMPTOM;
import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.CHOOSE_ADDING_FRAGMENT;

public class AddingActivity extends AppCompatActivity {

    private ViewHandler viewHandler;
    private ActionBar actionBar;
    private CancelDialog dialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewHandler = new ViewHandler(this);
        actionBar = getSupportActionBar();

        Intent intent = getIntent();
        if (intent != null) {
            chooseFragment(intent);
        }
    }

    private void chooseFragment(Intent intent) {
        String fragmentToSchow = intent.getStringExtra(CHOOSE_ADDING_FRAGMENT.toString());
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;
        String tag = "";
        Bundle bundle = new Bundle();
        if (intent.getBundleExtra(Keeper.BUNDLE.toString()) != null) {
            bundle = intent.getBundleExtra(Keeper.BUNDLE.toString());
        }
        if (fragmentToSchow.equals(ADD_FOOD.toString())) {
            fragment = new AddFoodFragment();
            tag = ADD_FOOD.toString();
            if ((intent.getBundleExtra(Keeper.BUNDLE.toString()) != null)) {
                fragment.setArguments(intent.getBundleExtra(Keeper.BUNDLE.toString()));
            }
            viewHandler.setTextInToolbar(getString(R.string.title_add_food), actionBar, true);
        } else if (fragmentToSchow.equals(ADD_SYMPTOM.toString())) {
            fragment = new AddSymptomFragment();
            tag = ADD_SYMPTOM.toString();
            viewHandler.setTextInToolbar(getString(R.string.title_add_symptom), actionBar, true);
        } else if (fragmentToSchow.equals(ADD_MEDICINE.toString())) {
            fragment = new AddMedicineFragment();
            tag = ADD_MEDICINE.toString();
            viewHandler.setTextInToolbar(getString(R.string.title_add_medicine), actionBar, true);
        } else if (fragmentToSchow.equals(ADD_NOTE.toString())) {
            fragment = new AddNoteFragment();
            tag = ADD_NOTE.toString();
            viewHandler.setTextInToolbar(getString(R.string.title_add_note), actionBar, true);
        }
        if (fragment != null) {
            try {
                if (bundle.getBoolean(Keeper.IS_UPDATE.toString())) {
                    fragment.setArguments(bundle);
                }
            } catch (NullPointerException e) {
                //nothing to do here
            }
            fragmentTransaction.replace(R.id.frame_layout_adding_activity, fragment, tag);
            fragmentTransaction.commit();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.nav_save:
                decideSaving();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        showCancelDialog();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void showCancelDialog() {
        dialogFragment = new CancelDialog();
        dialogFragment.show(getSupportFragmentManager(), "dialog ");
    }

    private void decideSaving() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout_adding_activity);
        if (fragment instanceof AddFoodFragment) {
            AddFoodFragment addFoodFragment = (AddFoodFragment) getSupportFragmentManager().findFragmentById(R.id.frame_layout_adding_activity);
            addFoodFragment.checkSave();
        } else if (fragment instanceof AddSymptomFragment) {
            AddSymptomFragment addSymptomFragment = (AddSymptomFragment) getSupportFragmentManager().findFragmentById(R.id.frame_layout_adding_activity);
            addSymptomFragment.checkSave();
        } else if (fragment instanceof AddMedicineFragment) {
            AddMedicineFragment addMedicineFragment = (AddMedicineFragment) getSupportFragmentManager().findFragmentById(R.id.frame_layout_adding_activity);
            addMedicineFragment.checkSave();
        } else if (fragment instanceof AddNoteFragment) {
            AddNoteFragment addNoteFragment = (AddNoteFragment) getSupportFragmentManager().findFragmentById(R.id.frame_layout_adding_activity);
            addNoteFragment.checkSave();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save, menu);
        return true;
    }


}
